def buildjar()
{
    echo "building jar.."
    sh "mvn package"
}

def buildimage()
{
     echo 'starting building image'
     withCredentials([usernamePassword(credentialsId:'docker-hub', usernameVariable: 'user', passwordVariable: 'password')]){     
     sh "docker build -t bhargavamurali/my-repo:jma-2.0 ."
     sh "docker login -u $user -p $password"
     sh "docker push bhargavamurali/my-repo:jma-2.0"
     echo "Building image done"
}
}

return this